.PHONY: image ctn-run

PORT ?= 2931

image:
	podman build . -t sha256le

ctn-run:
	podman run -p=$(PORT):80 sha256le
